// Copyright (c) 2018 atter developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! `atter` message
use std::fmt;
use uuid::Uuid;

/// Struct sent via tx to clients;
#[derive(Clone, Debug, Default, Deserialize, Getters, MutGetters, Serialize, Setters)]
pub struct Message {
    /// The unique message identifier.
    #[get = "pub"]
    #[set = "pub"]
    uuid: Uuid,
    /// The message.
    #[get = "pub"]
    #[set = "pub"]
    message: String,
}

impl fmt::Display for Message {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}: {}", self.uuid(), self.message())
    }
}

#[cfg(test)]
mod test {
    use super::Message;
    use bincode::{deserialize, serialize, Infinite};
    use uuid::{self, Uuid};

    const TEST_STR: &str = "test";
    const TEST_UUID: &str = "b428b5d9-df19-5bb9-a1dc-115e071b836c";

    const MSG_BYTES: [u8; 56] = [
        36, 0, 0, 0, 0, 0, 0, 0, 98, 52, 50, 56, 98, 53, 100, 57, 45, 100, 102, 49, 57, 45, 53, 98,
        98, 57, 45, 97, 49, 100, 99, 45, 49, 49, 53, 101, 48, 55, 49, 98, 56, 51, 54, 99, 4, 0, 0,
        0, 0, 0, 0, 0, 116, 101, 115, 116,
    ];

    #[test]
    fn serialize_message() {
        let mut message: Message = Default::default();

        message.set_uuid(Uuid::new_v5(&uuid::NAMESPACE_OID, TEST_STR));
        message.set_message(TEST_STR.to_string());

        let msg_bytes = serialize(&message, Infinite).expect("unable to serialize message");
        let mut expected: Vec<u8> = Vec::new();
        expected.extend(MSG_BYTES.iter());
        assert_eq!(msg_bytes, expected);
    }

    #[test]
    fn deserialize_message() {
        let message: Message = deserialize(&MSG_BYTES).expect("unable to deserialize message");
        assert_eq!(message.uuid().to_string(), TEST_UUID);
        assert_eq!(message.message(), TEST_STR);
    }
}
