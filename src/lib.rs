// Copyright (c) 2018 atter developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Configuration management for atter.
//!
//! # Examples
//!
//! ## Read from TOML string
//!
//! ```
//! # #[macro_use] extern crate error_chain;
//! # extern crate atter;
//! # extern crate toml;
//! #
//! # use atter::{read_toml, Config};
//! # use std::io::Cursor;
//! # use std::path::PathBuf;
//! # use std::time::Duration;
//! #
//! # mod error {
//! #     error_chain!{
//! #         foreign_links {
//! #             Io(::std::io::Error);
//! #             Atter(::atter::Error);
//! #             TomlDe(::toml::de::Error);
//! #             TomlSer(::toml::ser::Error);
//! #         }
//! #     }
//! # }
//! #
//! # fn main() {
//! #     read().expect("")
//! # }
//! #
//! # fn read() -> error::Result<()> {
//!     let test_toml = r#"[common]
//! base_db_path = "/Users/kon8116/projects/kon8116/atters"
//!
//! [[index]]
//! idx_prefix = "MslCheckout"
//!
//! [[index.fields]]
//! name = "id"
//! data_type = "Integer"
//! primary_key = true
//!
//! [[index.fields]]
//! name = "correlation_id"
//! data_type = "Text"
//! not_null = true
//!
//! [[index]]
//! idx_prefix = "ProxyApi"
//!
//! [[index.fields]]
//! name = "id"
//! data_type = "Integer"
//! primary_key = true
//!
//! [[index.fields]]
//! name = "correlation_id"
//! data_type = "Text"
//! not_null = true
//!
//! [prod]
//! echo_env_affix = "-digital"
//! verbose = false
//!
//! [prod.duration]
//! secs = 900
//! nanos = 0
//!
//! [stage]
//! verbose = false
//!
//! [stage.duration]
//! secs = 900
//! nanos = 0
//!
//! [test]
//! verbose = true
//!
//! [test.duration]
//! secs = 900
//! nanos = 0
//!
//! [dev]
//! verbose = true
//!
//! [dev.duration]
//! secs = 900
//! nanos = 0
//! "#;
//!
//!     // Serialize the TOML above into a `Config` struct.
//!     let mut reader = Cursor::new(test_toml);
//!     let config = read_toml(&mut reader)?;
//!
//!     // Check the `Config` struct.
//!     let common = config.common();
//!     let index = config.index();
//!     let prod = config.prod();
//!     let stage = config.stage();
//!     let test = config.test();
//!     let dev = config.dev();
//!
//!     assert_eq!(
//!        common.base_db_path(),
//!        &PathBuf::from("/Users/kon8116/projects/kon8116/atters")
//!     );
//!     assert_eq!(common.search_prefix(), "http://echo".to_string());
//!     assert_eq!(common.search_affix(), ".kroger.com/elastic".to_string());
//!     assert_eq!(common.search_suffix(), "/_search".to_string());
//!     assert_eq!(common.db_prefix(), "ordmon".to_string());
//!     assert_eq!(common.db_suffix(), ".db".to_string());
//!     assert_eq!(index.len(), 2);
//!
//! #   Ok(())
//! # }
//! ```
//!
//! ## Write to TOML string
//!
//! ```
//! # #[macro_use] extern crate error_chain;
//! # extern crate atter;
//! # extern crate toml;
//! #
//! # use atter::{write_toml, Common, Config, EchoIndex, Environment, Field, Index, SqliteDataType};
//! # use std::io::Cursor;
//! # use std::path::PathBuf;
//! # use std::time::Duration;
//! #
//! # mod error {
//! #     error_chain!{
//! #         foreign_links {
//! #             Io(::std::io::Error);
//! #             Atter(::atter::Error);
//! #             TomlDe(::toml::de::Error);
//! #             TomlSer(::toml::ser::Error);
//! #         }
//! #     }
//! # }
//! #
//! # const TEST_TOML: &str = r#"[common]
//! # base_db_path = "/Users/kon8116/projects/kon8116/atters"
//! #
//! # [[index]]
//! # idx_prefix = "MslCheckout"
//! #
//! # [[index.fields]]
//! # name = "id"
//! # data_type = "Integer"
//! # primary_key = true
//! #
//! # [[index.fields]]
//! # name = "correlation_id"
//! # data_type = "Text"
//! # not_null = true
//! #
//! # [[index]]
//! # idx_prefix = "ProxyApi"
//! #
//! # [[index.fields]]
//! # name = "id"
//! # data_type = "Integer"
//! # primary_key = true
//! #
//! # [[index.fields]]
//! # name = "correlation_id"
//! # data_type = "Text"
//! # not_null = true
//! #
//! # [prod]
//! # echo_env_affix = "-digital"
//! # verbose = false
//! #
//! # [prod.duration]
//! # secs = 900
//! # nanos = 0
//! #
//! # [stage]
//! # verbose = false
//! #
//! # [stage.duration]
//! # secs = 900
//! # nanos = 0
//! #
//! # [test]
//! # verbose = true
//! #
//! # [test.duration]
//! # secs = 900
//! # nanos = 0
//! #
//! # [dev]
//! # verbose = true
//! #
//! # [dev.duration]
//! # secs = 900
//! # nanos = 0
//! # "#;
//! #
//! # fn main() {
//! #     write().expect("unable to write");
//! # }
//! #
//! # fn write() -> error::Result<()> {
//!       // Setup the `Config` struct.
//!       let mut config: Config = Default::default();
//!       let mut common: Common = Default::default();
//!       let mut msl_checkout_index: Index = Default::default();
//!       let mut proxy_api_index: Index = Default::default();
//!       let mut prod: Environment = Default::default();
//!       let mut stage: Environment = Default::default();
//!       let mut test: Environment = Default::default();
//!       let mut dev: Environment = Default::default();
//!
//!       common.set_base_db_path(PathBuf::from("/Users/kon8116/projects/kon8116/atters"));
//!
//!       let mut id: Field = Default::default();
//!       id.set_name("id".to_string());
//!       id.set_primary_key(Some(true));
//!
//!       let mut correlation_id: Field = Default::default();
//!       correlation_id.set_name("correlation_id".to_string());
//!       correlation_id.set_data_type(SqliteDataType::Text);
//!       correlation_id.set_not_null(Some(true));
//!
//!       msl_checkout_index.set_fields(vec![id.clone(), correlation_id.clone()]);
//!
//!       proxy_api_index.set_idx_prefix(EchoIndex::ProxyApi);
//!       proxy_api_index.set_fields(vec![id, correlation_id]);
//!
//!       prod.set_echo_env_affix(Some("-digital".to_string()));
//!       prod.set_verbose(false);
//!       prod.set_duration(Duration::from_millis(900000));
//!
//!       stage.set_verbose(false);
//!       stage.set_duration(Duration::from_millis(900000));
//!
//!       test.set_verbose(true);
//!       test.set_duration(Duration::from_millis(900000));
//!
//!       dev.set_verbose(true);
//!       dev.set_duration(Duration::from_millis(900000));
//!
//!       config.set_common(common);
//!       config.set_index(vec![msl_checkout_index, proxy_api_index]);
//!       config.set_prod(prod);
//!       config.set_stage(stage);
//!       config.set_test(test);
//!       config.set_dev(dev);
//!
//!       // Write the TOML to the given buf.
//!       let mut buf = [0; 5000];
//!
//!       // Wrapped to drop mutable borrow.
//!       {
//!         let mut writer = Cursor::new(&mut buf[..]);
//!         write_toml(&config, &mut writer)?;
//!       }
//!
//!       // Check that the result is the same as the TOML above.
//!       let filtered = buf.iter().filter(|x| **x > 0).cloned().collect::<Vec<u8>>();
//!       assert_eq!(
//!           TEST_TOML,
//!           String::from_utf8(filtered).expect("Invalid UTF-8 in result")
//!       );
//! #   Ok(())
//! # }
//! ```
//!
#![feature(try_from)]
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate getset;
#[macro_use]
extern crate serde_derive;

#[cfg(test)]
extern crate bincode;
extern crate chrono;
extern crate regex;
extern crate rusqlite;
extern crate toml;
extern crate url;
extern crate uuid;

mod config;
mod dal;
mod error;
mod message;

pub use config::{db_path, echo_url, read_toml, scroll_url, write_toml, Common, Config, EchoIndex,
                 Environment, Field, Index, RuntimeEnvironment, SqliteDataType};
pub use dal::sqlite::{find_most_recent, initialize};
pub use error::Error;
pub use message::Message;
